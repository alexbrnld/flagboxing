<?php 
/**
  * This file displays the footer for the theme 
  *
  *
  * @package Wordpress - xela
  * @subpackage xela
  * @since xela 1.0
  *
**/
?>

    </div> <!-- default_sitecontent -->
    <!-- Footer -->
    <footer>

        <?php  
            $page_id = 2934;

            $args = array(
                'page_id' => $page_id
            );

            $query = new WP_Query($args);

            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();

                    the_content();
                }
            } 

            wp_reset_query();

        ?>

    	<div class="container-fluid"> 
    		<div class="row footer-container">
    			<div class="col-md-4">
                    <div class="footer-pdfs">
                        <a target="_blank" title="Privacy Policy" href="https://s3-ap-southeast-1.amazonaws.com/flagboxing/wp-content/uploads/2018/01/03120531/export.pdf" class="footer-text"> PRIVACY POLICY </a> <span class="ft-separator"> | </span><a target="_blank" title="Terms & Conditions" href="https://s3-ap-southeast-1.amazonaws.com/flagboxing/wp-content/uploads/2018/01/04075806/TERMSCONDITIONS.pdf" class="footer-text"> TERMS &amp; CONDITIONS </a> 
                    </div>
                    <h3 class="footer-text">Copyright. Flagboxing &copy; 2017</h3>
    			</div>
    			<div class="col-md-4">
                    <div class="footer-scmedia">
                        <ul class="social-inHd">
                            <li>
                                <a class="vc_icon_element-link" href="https://www.facebook.com/flagboxing/" title="" target=" _blank">
                                    <span class="vc_icon_element-icon fa fa-facebook"></span>
                                </a>
                            </li>
                            <li>
                                <a class="vc_icon_element-link" href="https://www.instagram.com/flagboxing/" title="" target=" _blank">
                                    <span class="vc_icon_element-icon fa fa-instagram"></span>
                                </a>
                            </li>
                            <li>
                                <a class="vc_icon_element-link" href="#" title="" target=" _blank">
                                    <span class="vc_icon_element-icon fa fa-snapchat-ghost"></span>
                                </a>
                            </li>
                            <li>
                                <a class="vc_icon_element-link" href="#" title="" target=" _blank">
                                    <span class="vc_icon_element-icon fa fa-youtube"></span>
                                </a>
                            </li>
                            <li>
                                <a class="vc_icon_element-link" href="#" title="" target=" _blank">
                                    <span class="vc_icon_element-icon fa fa-twitter"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="copyright">
                        <p>
                            Developed by: <a href="http://sarchitech.com">Sarchitech</a>
                        </p>
                    </div>
                </div>
    		</div>
    	</div>
        <!-- BACK TO TOP -->
        <!--   <a href="javascript:void(0)" id="back-to-top">
            <i class="fa fa-angle-up" aria-hidden="true"></i>
        </a> -->


    </footer>


    <?=wp_footer()?>
    <script type="text/javascript">

    // global variable for the player
      var tag = document.createElement('script');
      tag.id = 'iframe-demo';
      tag.src = 'https://www.youtube.com/iframe_api';
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;

    // this function gets called when API is ready to use
    function onYouTubePlayerAPIReady() {
      // create the global player from the specific iframe (#video)
      player = new YT.Player('featuredvideoid', {
        events: {
          // call this function when player is ready to use
          'onReady': onPlayerReady
        }
      });
    }

    function onPlayerReady(event) {
      jQuery("#play-button").addClass('disabled-button');
      jQuery("#mute-button").addClass('disabled-button');
      event.target.playVideo();
      // bind events
      var unMuteButton = document.getElementById("unmute-button");
      unMuteButton.addEventListener("click", function() {
        player.mute();
        jQuery("#mute-button").removeClass('disabled-button')
        jQuery("#unmute-button").addClass('disabled-button')
      });
      
      var muteButton = document.getElementById("mute-button");
      muteButton.addEventListener("click", function() {
        player.unMute();
        jQuery("#mute-button").addClass('disabled-button')
        jQuery("#unmute-button").removeClass('disabled-button')
      });
  
      // bind events
      var playButton = document.getElementById("play-button");
      playButton.addEventListener("click", function() {
        player.playVideo();
        jQuery("#play-button").addClass('disabled-button')
        jQuery("#pause-button").removeClass('disabled-button')
      });
      
      var pauseButton = document.getElementById("pause-button");
      pauseButton.addEventListener("click", function() {
        player.pauseVideo();
        jQuery("#play-button").removeClass('disabled-button')
        jQuery("#pause-button").addClass('disabled-button')
      });
  
   }
  </script>
  <script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type  function
*  @date  8/11/2013
*  @since 4.3.0
*
*  @param $el (jQuery element)
*  @return  n/a
*/

function new_map( $el ) {
  
  // var
  var $markers = $el.find('.marker');
  
  
  // vars
  var args = {
    zoom    : 16,
    center    : new google.maps.LatLng(0, 0),
    mapTypeId : google.maps.MapTypeId.ROADMAP
  };
  
  
  // create map           
  var map = new google.maps.Map( $el[0], args);
  
  
  // add a markers reference
  map.markers = [];
  
  
  // add markers
  $markers.each(function(){
    
      add_marker( $(this), map );
    
  });
  
  
  // center map
  center_map( map );
  
  
  // return
  return map;
  
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type  function
*  @date  8/11/2013
*  @since 4.3.0
*
*  @param $marker (jQuery element)
*  @param map (Google Map object)
*  @return  n/a
*/

function add_marker( $marker, map ) {

  // var
  var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

  // create marker
  var marker = new google.maps.Marker({
    position  : latlng,
    map     : map,
    icon: 'https://flagboxing.com/wp-content/themes/flag-boxing/assets/img/flagboxing-map.png'
  });

  // add to array
  map.markers.push( marker );

  // if marker contains HTML, add it to an infoWindow
  if( $marker.html() )
  {
    // create info window
    var infowindow = new google.maps.InfoWindow({
      content   : $marker.html()
    });

    // show info window when marker is clicked
    google.maps.event.addListener(marker, 'click', function() {

      infowindow.open( map, marker );

    });
  }

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type  function
*  @date  8/11/2013
*  @since 4.3.0
*
*  @param map (Google Map object)
*  @return  n/a
*/

function center_map( map ) {

  // vars
  var bounds = new google.maps.LatLngBounds();

  // loop through all markers and create bounds
  $.each( map.markers, function( i, marker ){

    var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

    bounds.extend( latlng );

  });

  // only 1 marker?
  if( map.markers.length == 1 )
  {
    // set center of map
      map.setCenter( bounds.getCenter() );
      map.setZoom( 16 );
  }
  else
  {
    // fit to bounds
    map.fitBounds( bounds );
  }

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type  function
*  @date  8/11/2013
*  @since 5.0.0
*
*  @param n/a
*  @return  n/a
*/
// global var
var map = null;

$(document).ready(function(){

  $('.acf-map').each(function(){

    // create map
    map = new_map( $(this) );

  });

});

})(jQuery);
</script>
</body>
</html>
