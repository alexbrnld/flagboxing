jQuery(function(){

    /* NAV MOBILE */
    jQuery(document).on('click', '.fbx-wrappermn li.sub-menu-mobile > span', function(event) {
        var listParent = jQuery(this).parent('li.sub-menu-mobile');
        if (listParent.find('ul.sub-menu').hasClass('position-static')) {
            listParent.removeClass('close-menu');
            listParent.addClass('open-menu');
            listParent.find('ul.sub-menu').removeClass('position-static');
        } else {
            listParent.removeClass('open-menu');
            listParent.addClass('close-menu');
            listParent.find('ul.sub-menu').addClass('position-static');
        }
        console.log(listParent, "Adasd");
    });

    jQuery('.fbx-menu > li.sub-menu-mobile').each(function(index, el) {
        if ($(this).find('span').length <= 0) {
            jQuery(this).append('<span></span>');
        }
    });

    jQuery('.nav-icon').click(function(){
        jQuery(this).toggleClass('open');
        if (jQuery( '.fbx-wrappermn' ).hasClass( 'shw' )) {
            jQuery('.fbx-wrappermn').removeClass('shw');
            jQuery('body').css('overflow-y','scroll');
        } else {
            jQuery('.fbx-wrappermn').addClass('shw');
            jQuery('body').css('overflow','hidden');
        }
    });

    var visits = jQuery.cookie('visits') || 0;


    jQuery('a#close-app').click(function(){
      jQuery(this).closest('div.same-close').hide();
        visits = 1;
        jQuery.cookie('visits', visits, { expires: 2/24, path: '/' });
    });

    jQuery('.coming-soon').click(function(event) {
        event.preventDefault();
        jQuery('.coming-soon-popup').fadeIn('500');
    });


    jQuery(window).scroll(function() {  
        var scroll = jQuery(window).scrollTop();

        if ($("#home_coches").length > 0) {
            var distance = $("#home_coches").offset().top + 150;
            if (scroll >= distance && visits < 1) {
                jQuery('.get-the-app').fadeIn('500');
                console.log('x');
            }
        } else {
            if (visits < 1) {
                jQuery('.get-the-app').fadeIn('500');
                console.log('y');
            }
        }

    });

    //play when video is visible
    jQuery('.hmClass-carousel').owlCarousel({
        margin:10,
        nav:true,
        navText: [arrowLeft, arrowRight],
        dots:false,
        responsive:{
            0:{
              items:1
            },
            600:{
              items:2
            },
            1000:{
              items:3
            },
            1400:{
              items:4
            }
        }
    });

    jQuery('.all-media-news').owlCarousel({
        autoplay: true,
        nav:true,
        navText: [arrowLeft, arrowRight],
        autoplayHoverPause: true,
        margin:10,
        dots:false,
        responsive:{
            0:{
              items:1
            },
            600:{
              items:3
            },
            1000:{
              items:5
            }
        }
    });

    jQuery("footer .footer-topsection h2").click(function(event) {
        var sectionFooter = $(this).closest("div.wpb_wrapper");
        var sectionClass = $(this).attr('id');
        if (jQuery('.' +sectionClass+ ' ul.menu').hasClass('showmenu')) {
            jQuery('.' +sectionClass+ ' ul.menu').removeClass('showmenu')
            $(this).removeClass('shownow');
        } else {
            $(this).addClass('shownow');
            jQuery('.' +sectionClass+ ' ul.menu').addClass('showmenu')
        }
    });

    jQuery('#qty-input').on('change', function(){
        var qtyValue = jQuery(this).val();
        var addToCartHref = $('#addTocart-id').attr('href');
        var stripped = addToCartHref.substring(0, addToCartHref.indexOf('&quantity=') + '&quantity='.length);

        $('#addTocart-id').attr('href', stripped+qtyValue); 
    })


    var winWidth = $(window).width();

    if (winWidth > 767) {
        setTimeout(function() {
            jQuery('.home-featured-text').fadeOut();
        }, 5000);
    }

    jQuery('.all-packages-dropdown li a').click(function(){
        var selectedPackage = jQuery(this).text();
        console.log(selectedPackage);
        jQuery('#packages').text(selectedPackage);
    })

});


jQuery(window).load(function(){


    var url = window.location.href; 
    var hash = window.location.hash;


    if (hash != "") {
        jQuery("html, body").animate({
            scrollTop: parseInt(jQuery(hash).offset().top) - 180 
        }, 1000);
    }

    jQuery('.home-coaches img').attr('sizes', '');

    var isTabLoaded = sessionStorage.getItem("isTabLoaded") == "true";
    var isUserBrowserIPad = navigator.userAgent.indexOf('iPad') > -1
    if (!isTabLoaded && isUserBrowserIPad) {
        window.location.reload();
        sessionStorage.setItem("isTabLoaded", "true");
    };

    if (homeID == 156) {

        var labelString = $( "label:contains('*')" );
        console.log(labelString);

        jQuery(".form-row label").each(function() {
             jQuery(this).html(jQuery(this).html().replace('*', '<abbr class="required" title="required">*</abbr>'));
         });

    }

});