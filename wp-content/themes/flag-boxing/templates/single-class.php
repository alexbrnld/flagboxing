<?=get_header()?>

<?php  
	
	$id = get_the_id();
	$_content = get_field('short_description');
	$course_outline = get_field('course_outline');
	$title = get_the_title();
	$wooTicket = get_post_meta( $id, '_wcs_woo_product', true );
	$instructor = get_post_meta( $id, 'wcs-instructor', true );
	$content = apply_filters( 'the_content', get_the_content() );
	$_product = wc_get_product( $wooTicket );
	$ce_price = $_product->get_price();

	$room = do_shortcode('[wcs_event_terms term="wcs-room"]');
	$dateAndTime = do_shortcode('[wcs_event_date]');
	$map = do_shortcode( '[wcs_event_map]' );

	// New Location Map Field
	$workshop_location = get_field('workshop_map');
	$workshop_location_text = get_field('workshop_location');

	// Terms and Conditions
	$terms_and_conditions = get_field('terms_and_conditions');
	

    $timestamps = get_post_meta( $id, '_wcs_timestamp', true );

   	// GET POST CLASS TYPE
   	$terms = get_the_terms( $id , 'wcs-type' );
   	$term_name = "";

   	$workshop = false;
   	$classes = false;
   	$invalid = false;

   	if ( $terms != null ){
   		$term_ids = array();
		foreach( $terms as $term ) {
			$term_ids[] = $term->term_id;
			if ($term->slug == "workshops") {
				$workshop = true;
				$term_name = $term->name;
			} elseif($term->slug == "classes") {
				$classes = true;
				$term_name = $term->name;
			} else {
   				$invalid = true;
			}
		} 
	};

    // if Woo Ticket Capacity is empty = CLASS(class type or category), if not, = WORKSHOPS(class type or category)
    $wooTicketCapacity = get_post_meta( $id, '_wcs_woo_capacity', true );
    $total_sales = $_product->total_sales;

    // echo "<p style='display:none'>". $total_sales."</p>";

    // GET REMAINING CAPACITY FOR WORKSHOPS
    $remaining_capacity = $wooTicketCapacity - $total_sales;
    $remaining_capacity = abs($remaining_capacity);

?>

	<div class="pg-product-single"> 

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">	
					<?=do_shortcode('[wcs_event_image]')?>
				</div>
				<div class="col-md-6">	
					<div class="ce-details">
						<h1 class="ce-title"><?=$title?></h1>
						<h2 class="ce-price">SAR <?=$ce_price?>.00</h2>
						<h4 class="ce-instructor">Instructor: <?=do_shortcode('[wcs_event_terms term="wcs-instructor"]')?></h4>
						<br>
						<div class="ce-content">
							<?php  
								echo $_content;
							?>
						</div>
						<br>	

						<!-- CLASS -->
						<?php if ($classes === true): ?>
							<!-- Terms and Conditions -->
							<div class="termsAndConditions">
								<h3 class="ce-title">Terms &amp; Conditions</h3>
								<div class="ce-content">
									- Payment for classes is done through the website.<br>

									- All class bookings are through <a href="https://flagboxing.com/join-us/">Flagboxing Mobile app</a> <br>

									- You will not be able to attend class without prior booking <br>

									- We do not refund payments<br>

									- You must be over 18 years old to pay<br>
								</div>
							</div>

							<!-- Get Packages -->
							<?php  
								$args = array(
								    'hide_empty' => false, 
								    'parent' => 48
								);

								$terms = get_terms('product_cat', $args);
								
								$class_single_cart = site_url(). "/cart/?add-to-cart=" .$wooTicket. "&event=" .$id. "&timestamp=" .$timestamps. "&quantity=1";
							?>
							<br>
							<div class="row">
								<div class="col-lg-5">
									<div class="single-addtocart">
										<div class="packages-titlein">Single Session</div>
										<div class="single-itocart">
											<!-- <a href="#" title="Add <?=$title?> to cart" class="coming-soon"><?=wc_price($ce_price)?></a> -->
											<a href="<?=$class_single_cart?>" title="Add <?=$title?> to cart"><?=wc_price($ce_price)?></a>
										</div>
									</div>
								</div>
								<div class="col-lg-7">
									<?php if (!empty($terms)): ?>
										<div class="pakcages-details">
											<div class="packages-titlein">Packages</div>
											<div class="dropdown">
												<button class="btn btn-default dropdown-toggle packages-header" type="button" id="packages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													Select Package
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu all-packages-dropdown" aria-labelledby="packages">
													<?php if (!empty($terms)): ?>
													<?php foreach ($terms as $term): ?>
														<li class="<?=$term->slug?>">
															<a href="<?=site_url()?>/packages/#<?=$term->slug?>_<?=$term->term_id?>"><?=$term->name?></a>
														</li>
														<?php endforeach ?>
													<?php endif ?>
												</ul>
											</div>
										</div>
									<?php endif ?>
								</div>
							</div>

						<?php endif ?>

						<!-- WORKSHOPS -->
						<?php if ($workshop === true): ?>
							<?php if (!empty($dateAndTime)): ?>
								<h3 class="ce-title">DATE AND TIME</h3>
								<div class="ce-content">
									<?=do_shortcode('[wcs_event_date]')?>
								</div>
							<br>
							<?php endif ?>
							<?php if (!empty($ce_price)): ?>
								<h3 class="ce-title">PRICE</h3>
								<div class="ce-content">
									<?=$ce_price?> SAR (non refundable)
								</div>
							<br>
							<?php endif ?>
							<?php if ($remaining_capacity != 0): ?>
								<h3><?=$class_type?><?=$remaining_capacity?> places remaining</h3>
							<?php endif ?>

							<?php  
								// Default - REMOVE FOR THE MEANTIME
								$add_to_cart = site_url(). "/cart/?add-to-cart=" .$wooTicket. "&event=" .$id. "&timestamp=" .$timestamps. "&quantity=1";
								// On progress
								// $add_to_cart = "";
							?>

							<?php if ($remaining_capacity != 0): ?>
								<input id="qty-input" type="number" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
		                        <!-- <a id="addTocart-id" class="coming-soon" href="<?=$add_to_cart?>" title="">ADD TO CART</a> -->
		                        <a id="addTocart-id" href="<?=$add_to_cart?>" title="">ADD TO CART</a>
		                    <?php elseif($remaining_capacity == 0 && !empty($wooTicketCapacity)): ?>
		                        <!-- <h4 class="event-sold-out">SOLD OUT</h4> -->
		                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/sold-out-png-30.png" class="sold-out_icon">
							<?php endif ?>
						<?php endif ?>


					</div>
				</div>
			</div>	

		</div>
		<?php if ($workshop === true): ?>
			<div class="course-outline">
				<div class="row">
					<div class="col-md-12">	
						<h3 class="ce-title">LOCATION</h3>
						<?php if (!empty($workshop_location_text)): ?>
							<?=$workshop_location_text?>
						<?php endif ?>
						<?php if (!empty($workshop_location)): ?>
							<div class="ce-content classtype-location">
								<?php
								if( !empty($workshop_location) ):
								?>
								<div class="acf-map">
									<div class="marker" data-lat="<?php echo $workshop_location['lat']; ?>" data-lng="<?php echo $workshop_location['lng']; ?>"></div>
								</div>
								<?php endif; ?>
							</div>
						<?php endif ?>
						<br><br>
						<?php if (!empty($course_outline)): ?>
							<h3 class="ce-title">COURSE OUTLINE</h3>
							<div class="ce-content">
								<?=$course_outline?>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		<?php endif ?>

	</div>

	<?php if ($classes === true): ?>
		<div class="pg-reated-class">
			<h3 class="ce-title">Related <?=$term_name?></h3>
			<br>

			<?php if (count($term_ids) == 1): ?>
				<?php foreach ($term_ids as $term_id): ?>
					<?php  
						$termID = $term_id;
					?>
				<?php endforeach ?>
			<?php endif ?>

			<?=do_shortcode('[classCarousel classtype=true classtype_category='.$termID.' posts_per_page=5 post__not_in='.$id.']') ?>
		</div>
	<?php endif ?>
<?=get_footer()?>