<?php 
/**
  * This file displays the header for the theme 
  * Displays all of the head element and everything
  *
  *
  * @package Wordpress - xela
  * @subpackage xela
  * @since xela 1.0
  *
**/
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title() ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/html5.js"></script>
    <![endif]-->
    <style> 

    	.media-coverage-logo .vc_images_carousel {
    		width: 100% !important;
    	}

    	.media-coverage-logo .vc_images_carousel.vc_build .vc_carousel-slideline .vc_carousel-slideline-inner>.vc_item img {
    		filter: grayscale(100%);
    		transition: .8s;
    	}

    	.media-coverage-logo .vc_images_carousel.vc_build .vc_carousel-slideline .vc_carousel-slideline-inner>.vc_item img:hover {
    		filter: grayscale(0);
    	}


    	.media-coverage-logo .vc_images_carousel .vc_carousel-control {
    		background-color: #262626;
    		height: 30px;
		    width: 30px;
		    color: #fff;
		    text-shadow: none;
		    font-size: 65px;
		    opacity: 1;
    	}

    	.media-coverage-logo .vc_images_carousel .vc_carousel-control.vc_left {
    		left: -5%;
    	}

    	.media-coverage-logo .vc_images_carousel .vc_carousel-control.vc_right{
    		right: -5%;
    	}

		.fbx-logo img {
		    height: 100px;
		    width: auto;
		}


        .news-items {
            padding: 0 !important;
        }
        
        .media-thead-n {
            font-size: 30px;
            line-height: 15px;
            font-style: normal;
            font-weight: 600;
            letter-spacing: 2.7px;
            text-transform: uppercase;
            color: #222222;
            border: none;
            padding: 12px 5px;
            font-family: DINBold, sans-serif;
            text-align: left;
            margin-top:100px;
            margin-bottom: 30px;
        }
       
        .vc_icon_element.vc_icon_element-outer.icons-footer {
            display: inline-block !important;
            margin-left: 5px !important;
        }
        
        .bottom-footer {
            padding: 0 !important;
        }
        
      /*  .language-menu ul li:last-child {
            display: none !important;
        }*/
        
        .language-menu ul li {
        	margin-right: 6px !important;
        }

        .language-menu ul li a.active {
            color: #fff !important;
            border:  2px solid #fff !important;
            padding: 3px 5px !important;
        }

        .language-menu ul li a {
        	font-size: 15px !important;
            color: #fff !important;
        } 

        .top-header {
        	display: none !important;
        }

        /* DEAKTOP HEADER */

        .large-header {
        	background-color: rgba(0, 0, 0, .9);
        	position: fixed;
        	left: 0;
        	right: 0;
        	top: 0;
        	min-height: 70px;
        	width: 100%;
        	z-index: 9999;
        	padding: 0 2%;
        }

        .lgheader-logo img {
        	height: 80px;
        }

        .lgheader-menu, .language-wrap {
        	height: 80px;
		    display: table;
		    width: 100%;
		    text-align: right;
        }

        .lgheader-menu ul, .language-wrap > div {
    	    list-style: none;
		    padding: 0;
		    margin: 0;
			display: table-cell;
			width: 100%;
			height: 100%;
			vertical-align: middle;
        }

        .lgheader-menu ul li {
        	display: inline-block;
   			margin: 0 10px;
        }

        .lgheader-menu ul li a {
        	color: #fff;
		    text-transform: uppercase;
		    letter-spacing: 1.2px;
		    font-size: 14px;
		    transition: 1s;
        }

        .lgheader-menu ul li a:hover, 
        .lgheader-menu ul li a:focus {
        	color: #23b3d2;
        }

        .download-app a {
        	border: 3px solid #23b3d2;
		    padding: 10px;
		    color: aliceblue;
        }

	        @media (max-width: 1224px) {

	        	.lgheader-menu ul li a {
	        		font-size: 12px;
	        	}

	        	.lgheader-menu ul li {
	        		margin: 0 6px;
	        	}

	        	.language-menu ul li a.active {
	        		padding: 2px 4px;
	        	}

	        	.language-menu ul li a {
	        		font-size: 13px;
	        	}

	        }

	        @media (max-width: 1040px) {
		        .top-header {
		        	display: block !important;
		        }

		        .large-header {
		        	display: none;
		        }

		        .language-wrap {
		        	height: auto;
		        	width: auto;
		        	display: block;
		        }

		        .language-menu ul li a.active {
		        	padding: 2px 5px;
		        }

		        .language-menu ul li a {
		        	font-size: 20px;
		        }

		        .menu-br {
		        	top: 20px;
		        	right: 0;
		        }

		        .menu-br>div {
		        	line-height: 40px;
		        	vertical-align: bottom;
		        }

		        .fbx-wrappermn li a {
		        	padding: 5px 8px;
		        }

		        .download-app a {
		        	border:  none;
		        	color: #fff;
		        }

		        .fbx-logo img {
		        	height: 70px;
		        }

		        .top-header {
		        	background-color: rgba(0, 0, 0, .8);
    				height: 80px;
		        }

		        .fbx-logo {
		        	top: 5px;
		        }
	        }
    </style>
    <?=wp_head()?>
</head>
<body <?php body_class(); ?> > 
    <header>
    	<!-- DESKTOP -->
    	<div class="large-header">
    		<div class="largeheader-inner">
    			<div class="lgheader-wrapper">
    				<div class="row">
    					<div class="col-md-1">
    						<div class="lgheader-logo">
	                            <a href="<?=get_site_url()?>" title="Flag Boxing logo" class="flag-boxing-logo">
	                                <img src="https://s3-ap-southeast-1.amazonaws.com/flagboxing/wp-content/uploads/2017/11/30021547/flag-boxing-logo.png">
	                            </a>
    						</div>
    					</div>
    					<div class="col-md-10">
    						<div class="lgheader-menu">
                                <?php  
                                    wp_nav_menu( array(
                                        'theme_location'  => 'primary-menu',
                                        'container'       => false,
                                        'menu_class'      => 'header-menu',
                                    ) );
                                ?>
    						</div>
    					</div>
    					<div class="col-md-1">
                            <div class="language-wrap">
                            	<?php  language_selector_flags(); ?>
                            </div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

        <!-- MOBILE -->
        <section class="top-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="fbx-logo">
                            <a href="<?=get_site_url()?>" title="Flag Boxing logo" class="flag-boxing-logo">
                                <img src="https://s3-ap-southeast-1.amazonaws.com/flagboxing/wp-content/uploads/2017/11/30021547/flag-boxing-logo.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-9">
                            <div class="menu-br">
                                <div class="language-wrap">
                                    <?php  language_selector_flags(); ?>
                                </div>
                                <div class="nav-icon">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                            <div class="fbx-wrappermn">
                                <div class="fbwrapperinner">
                                    <div class="wrapperin">                    
                                        <?php  
                                            wp_nav_menu( array(
                                                'theme_location'  => 'primary-menu',
                                                'container'       => false,
                                                'menu_class'      => 'fbx-menu',
                                            ) );
                                        ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </section>
    </header>

    <div class="default_sitecontent">
