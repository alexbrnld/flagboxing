<?php
/**
 * Functions and definitions
 *
 * The functions.php is file consists of functions that help you 
 * override default configurations in wordpress theme.
 *
 *
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 *
 * @package Wordpress - xela
 * @subpackage xela
 * @since xela 1.0
 *
 *
**/

/**
 *
 * NOW: Override default configuration for your wordpress theme.
 *
**/

/*
 * Enable support for Post Formats.
 *
 * See: https://codex.wordpress.org/Post_Formats
 */
// add_theme_support( 'post-formats', array(
//  'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
// ) );


if (function_exists('add_theme_support')) {


    //Enable support for Post Thumbnails on posts and pages.
    // add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-thumbnails', array( 'post', 'class' ) );   
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Cus

    //Enable support for Post Thumbnails on posts and pages.
    add_theme_support( 'widgets' );

}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// footer wp_nav_menu
function all_menus() {
    register_nav_menus( array(
        'primary-menu' => __( 'Primary Menu' ),
        'right-nav-menu' => __( 'Right Menu' ),
        'left-nav-menu' => __( 'Left Menu' ),
        'footer-menu' => __( 'Footer Menu' ),
        'classes-menu' => __( 'Classes Menu' ),
        'workshop-menu' => __( 'Workshops Menu' ),
        'other-links' => __( 'Other Links Menu' ),
        'language-switcher' => __( 'Language Switcher' )
    ));
}
add_action( 'init', 'all_menus'); // register wp_nav_menu

function menuclass_widgets_init() {

    register_sidebar( array(
        'name'          => 'Classes - Menu',
        'id'            => 'class-menu',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

}
add_action( 'widgets_init', 'menuclass_widgets_init' );

function wokshops_menu() {

    register_sidebar( array(
        'name'          => 'Workshops - Menu',
        'id'            => 'workshops-menu',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

}
add_action( 'widgets_init', 'wokshops_menu' );

function other_links() {

    register_sidebar( array(
        'name'          => 'Other Linkss - Menu',
        'id'            => 'other-links',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ) );

}
add_action( 'widgets_init', 'other_links' );
/**
  *
  * Add styles and scripts here. 
  * CSS / JS
  * Reminder: If your css is design to override another css, make sure it will the last style in order.
  *
**/

function import_scripts_and_styles() {

    /**
      * OHM and Xela used default Css Framework
      * Bootstrap v3.3.6 (http://getbootstrap.com)
      *
    **/
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '', false );
    wp_enqueue_style( 'owl.theme.default.min', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css', array(), '', false );
    wp_enqueue_style( 'owl.carousel.min', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '', false );
    wp_enqueue_style( 'custom-style.css', get_template_directory_uri() . '/assets/css/custom-style.css', array(), time(), false );
    wp_enqueue_style( 'product-csss', get_template_directory_uri() . '/assets/css/product-csss.css', array(), time(), false );

    // Overrides other css style
    wp_enqueue_style( 'custom-css' , get_template_directory_uri() . '/style.css', array(), time(), false );

    // Overrides other js scripts
    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '', false );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '', true );
    wp_enqueue_script( 'owl-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), '', true );
    wp_enqueue_script( 'jquery-cookie.js', get_template_directory_uri() . '/assets/js/jquery-cookie.js', array(), '', true );
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom-script.js', array(), time(), true );


    // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

}
add_action('wp_enqueue_scripts', 'import_scripts_and_styles');

// LIMIT EXCERPT
function custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// READ MORE [..]

function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

// Shortcode [classCarousel]
// classtype (true) = carosuel / classtype (false) = grid
// classtype_category = the class type ID
function class_carousel($atts) { 

    $classtype_atts = shortcode_atts( array(
        'classtype' => true,
        'classtype_category' => '',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'post__not_in' => ''
    ), $atts );

    if ($classtype_atts['classtype'] == 'false') {
        $wrapper = "container-fluid cw-grid";
        $wrapper_inner = "row";
        $content_wrapper = "col-md-4 col-sm-6 classtype-grid";
    }
    if ($classtype_atts['classtype'] == 'true'){
        $wrapper = "hmCarousel-wrapper cw-carousel";
        $wrapper_inner = "hmClass-carousel owl-carousel owl-theme";
        $content_wrapper = "";
    }

    $post_type = 'class';
    $category_type = (int)$classtype_atts['classtype_category'];
    $posts_per_page = (int)$classtype_atts['posts_per_page'];
    $orderby = $classtype_atts['orderby'];
    $post__not_in = explode(',', $classtype_atts['post__not_in']);

    $args = array(
        'posts_per_page' => $posts_per_page,
        'post_type' => 'class',
        'order' => 'DESC',
        'orderby' => $orderby,
        'post__not_in' => $post__not_in,
        'tax_query' => array(
            array(
                'taxonomy' => 'wcs-type',
                'field' => 'term_id',
                'terms' => $category_type,
            )
        )
    );

    if (empty($post__not_in)) {
        unset($args['post__not_in']);
    }

    if (empty($category_type)) {
        unset($args['tax_query']);
    }

    $query = new WP_Query($args);

    ?>
        <div class="<?=$wrapper?>">
            <div class="<?=$wrapper_inner?>">
                <?php

                    if ($query->have_posts()) {
                        while ($query->have_posts()) {
                            $query->the_post();

                                $id = get_the_id();
                                $title = get_the_title();
                                $excerpt = get_the_excerpt();
                                // $feature_img = do_shortcode('[wcs_event_image id='.$id.']');
                                $permalink = get_the_permalink();

                                $feature_img = get_post_meta( $id, '_wcs_image', true );
                                $timestamps = get_post_meta( $id, '_wcs_timestamp', true );
                                $wooTicket = get_post_meta( $id, '_wcs_woo_product', true );

                                // COMING SOON LINK
                                $book_now = "https://flagboxing.com/cart/?add-to-cart=".$wooTicket."&event=".$id."&timestamp=".$timestamps;
                                // $coming_soon_class = "";
                                // if ($category_type == 3) {
                                // 	$coming_soon_class = "coming-soon";
                                // } 

                                ?>
                                    <div class="<?=$content_wrapper?>" >
                                        <div class="cc-item item">
                                            <a href="<?=$permalink?>" title="<?=$title?>">
                                                <h3 class="cc-title"><?=$title?></h3>
                                                <div class="feature-imgwrapper">
                                                    <div class="feature-singImg" style="background-image:url(<?=$feature_img?>);">
                                                </div>
                                                </div>
                                                <div class="cc-excerpt">
                                                   <?=$excerpt?>
                                                </div>
                                            </a>
                                            <div class="cc-buttons">
                                                <div class="cc-learn-more">
                                                    <a href="<?=$permalink?>" title="">Learn more</a>
                                                </div>
                                                <div class="cc-book-now">
                                                    <a href="<?=$book_now?>" title="">Book now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php

                        }
                        wp_reset_postdata();
                    }
                ?>
            </div>
        </div>
    <?php

    wp_reset_query();

}

add_shortcode( 'classCarousel', 'class_carousel' );

function get_custom_post_type_template($single_template) {
     global $post;

     if ($post->post_type == 'class') {
          $single_template = locate_template('/templates/single-class.php');
     }
     return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' );

// PAGE - PACKAGES
// Get all packages in WOOCommerce with category = packages
// Shortcode [flagboxingPackages]

function flagboxing_packages($atts) {

	$packages = 48; // Parent ID
	$args = array('hide_empty' => false, 'parent' => $packages );

	$categories = get_terms('product_cat',$args);

	echo '<div class="package-list">';
		foreach ($categories as $cat) {
            $package_header_id = $cat->slug . '_' . $cat->term_id;
			echo '<div class="package-header" id="'.$package_header_id.'">';
				echo '<h2>' .$cat->name. '</h2>';
				echo '<p>' .$cat->description. '</p>';
			echo '</div>';

			echo '<div class="package-item">';

				$args = array(
					'post_type' => 'product',
					'orderby'   => 'meta_value_num',
				    'meta_key'  => '_price',
				    'order' => 'asc',
					'tax_query' => array(
						array(
							'taxonomy' => 'product_cat',
							'field'    => 'slug',
							'terms'    => $cat->slug, 
						),
					),
				);

				$query = new WP_Query($args);

				if ($query->have_posts()) {
					while ($query->have_posts()) {
					    $query->the_post();

					    $id = get_the_id();
                        $product_title = get_the_title();
					    $product_content = get_the_content();
                        $wooTicket = get_post_meta( $id, '_regular_price', true );

					    echo '<div class="package-item-inner">';
					    	echo '<h3>' .wc_price($wooTicket). '</h3>';
					    	echo '<p><strong>' .$product_content. '</strong></p>';
                            echo '<p><a href="'.site_url(). '/cart/?add-to-cart='.$id.'"><strong class="signup-btn">BUY NOW</strong></a></p>';
					    	// echo '<p><a href="" class="coming-soon"><strong class="signup-btn">Buy Now</strong></a></p>';
					    echo '</div>';

					}
				}

				wp_reset_query();

			echo '</div>';
			
		}
	echo '</div>';
}

add_shortcode( 'flagboxingPackages', 'flagboxing_packages' );


// Shortcode for feature-news

function flagboxing_feature_on($atts) {

	$post_type = "featured_on";

	$args = array(
		'post_type' => $post_type,
		'posts_per_page' => -1,
	);

	$query = new WP_Query($args);

	if ($query->have_posts()) {
		?> <div class="all-media-news owl-carousel owl-theme"> <?php
		while ($query->have_posts()) {
		 	$query->the_post();

		 	$title = get_the_title();
		 	$logo = get_field('logo');
		 	$link_url = get_field('link_to');

		 	?>
	 			<div class="media-item">
	 				<a href="<?=$link_url?>" title="<?=$title?>" target="_blank">
	 					<img src="<?=$logo?>" class="img-responsive">
	 				</a>
	 			</div>
		 	<?php

		}
		?> </div><?php
	}
	wp_reset_query();
}

add_shortcode( 'flagboxingFeatureNews', 'flagboxing_feature_on' );


add_filter( 'gform_confirmation_anchor', function() {
    return 20;
} );

add_filter( 'woocommerce_currency_symbol', 'change_currency_symbol', 10, 2 );

function change_currency_symbol( $symbols, $currency ) {
    if ( 'SAR' === $currency ) {
        return 'SAR';
    }

        return $symbols;
}

/**
 * this code will add an unsupported currency to Woocommerce (tested up to v3.1.2 on WP 4.8.2) 
 * it will convert any amounts including a cart discount, tax or shipping into a supported currency (in this case USD)
 * paypal HTML variables can be found at https://developer.paypal.com/webapps/developer/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/#id08A6HI00JQU
 * payment processing can then be done via Paypal
 * this example uses the UAE Dirham (or AED). Change the currency symbol and abbreviation to suit your environment.
 * note - any orders placed this way are automatically placed on-hold by Woocommerce due to an amount & currency mismatch and MUST be manually updated to processing or complete, esp with virtual or downloadable orders.
 * stock or inventory management does NOT work with this workaround - there is an additional step needed please see https://vinefruit.net/woocommerce-currency-tweak/ 
 * 
 * 3 October 2017: updated Google currency converter URL as the old one stopped working
 */
 
/*Step 1 Code to use AED currency to display Dirhams in WooCommerce:*/
// add_filter( 'woocommerce_currencies', 'add_aed_currency' );  
// function add_aed_currency( $currencies ) {  
//     $currencies['AED'] = __( 'UAE Dirham', 'woocommerce' );  
//     return $currencies;  
// }
/*Step 2 Code to add AED currency symbol in WooCommerce:*/
// add_filter('woocommerce_currency_symbol', 'add_aed_currency_symbol', 10, 2);  
// function add_aed_currency_symbol( $currency_symbol, $currency ) {  
//     switch( $currency ) {  
//         case 'AED': $currency_symbol = 'AED'; break;  
//     }  
//     return $currency_symbol;  
// }  
// add_filter( 'woocommerce_paypal_supported_currencies', 'add_aed_paypal_valid_currency' );       
// function add_aed_paypal_valid_currency( $currencies ) {    
//     array_push ( $currencies , 'SAR' );  
//     return $currencies;    
// }   
/*Step 3 – Code to change 'AED' currency to ‘USD’ before checking out with Paypal through WooCommerce:*/
// add_filter('woocommerce_paypal_args', 'convert_aed_to_usd', 11 );  
// function get_currency($from_Currency='USD', $to_Currency='SAR') {
//     $url = "https://finance.google.com/finance/converter?a=1&from=$from_Currency&to=$to_Currency";
//     $ch = curl_init();
//     $timeout = 0;
//     curl_setopt ($ch, CURLOPT_URL, $url);
//     curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
//     curl_setopt ($ch, CURLOPT_USERAGENT,
//                  "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
//     curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
//     $rawdata = curl_exec($ch);
//     curl_close($ch);
//     $data = explode('bld>', $rawdata);
//     $data = explode($to_Currency, $data[1]);
//     return round($data[0], 2);
// }
// function convert_aed_to_usd($paypal_args){
//     if ( $paypal_args['currency_code'] == 'SAR'){  
//         $convert_rate = get_currency(); //Set converting rate
//         $paypal_args['currency_code'] = 'USD'; //change AED to USD  
//         $i = 1;  
        
//         while (isset($paypal_args['amount_' . $i])) {  
//             $paypal_args['amount_' . $i] = round( $paypal_args['amount_' . $i] / $convert_rate, 2);
//             ++$i;  
//         }  
//         if ( $paypal_args['shipping_1'] > 0 ) {
//             $paypal_args['shipping_1'] = round( $paypal_args['shipping_1'] / $convert_rate, 2);
//         }
        
//         if ( $paypal_args['discount_amount_cart'] > 0 ) {
//             $paypal_args['discount_amount_cart'] = round( $paypal_args['discount_amount_cart'] / $convert_rate, 2);
//         }
//         if ( $paypal_args['tax_cart'] > 0 ) {
//             $paypal_args['tax_cart'] = round( $paypal_args['tax_cart'] / $convert_rate, 2);
//         }
//     }
// return $paypal_args;  
// }   

//remove_menu_page( 'edit-comments.php' );
//function my_remove_menu_pages() {

//global $user_ID;
//remove_submenu_page( 'edit.php','post-new.php' );


//if ( !current_user_can( 'administrator' ) ) {
//remove_menu_page( 'tools.php' );
//remove_menu_page( 'edit-comments.php' );
//remove_menu_page( 'vc-welcome' );
//remove_submenu_page( 'edit.php','post-new.php' );//
//}////
//}
//add_action( 'admin_init', 'my_remove_menu_pages' );


// http://www.php.net/manual/en/function.array-search.php#91365
function recursive_array_search_php_91365( $needle, $haystack ) 
{
    foreach( $haystack as $key => $value ) 
    {
        $current_key = $key;
        if( 
            $needle === $value 
            OR ( 
                is_array( $value )
                && recursive_array_search_php_91365( $needle, $value ) !== false 
            )
        ) 
        {
            return $current_key;
        }
    }
    return false;
}

// GOOGLE MAP API

function my_acf_google_map_api( $api ){
    
    $api['key'] = 'AIzaSyCigBweTzFJtTkfnu_1NkCkcIUfjkwDpYs';
    $api['callback'] = 'initMap';
    
    return $api;
    
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function add_gf_cap()
{
    $role = get_role( 'editor' );
    $role->add_cap( 'gform_full_access' );
}
 
add_action( 'admin_init', 'add_gf_cap' );


 
function disable_new_posts() {
		// Hide sidebar link
		global $submenu;
	

		global $user_ID;
		// Hide link on listing page
		if ( current_user_can( 'client' ) ) {
  unset($submenu['edit.php?post_type=class'][10]);
  unset($submenu['edit.php?post_type=featured_on'][10]);
echo '<style> li#menu-posts li#menu-posts ul li:last-child {
			display: none !important;
		} </style>';
if  (isset($_GET['post_type']) && $_GET['post_type'] == 'class') {
			echo '<style type="text/css">
			#favorite-actions, .add-new-h2, .tablenav, a.page-title-action,span.duplicate_class { }
			</style>';
}
if  (isset($_GET['post_type']) && $_GET['post_type'] == 'featured_on') {
                        echo '<style type="text/css">
                   #favorite-actions, .add-new-h2, .tablenav, a.page-title-action,span.duplicate_class {}
                        </style>';
}



		}


	}
	add_action('admin_menu', 'disable_new_posts');

function remove_menus(){
global $user_ID;

if ( !current_user_can( 'administrator' ) ) {

  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'jetpack' );                    //Jetpack* 
  remove_menu_page( 'upload.php' );                   //Posts
  //remove_menu_page( 'upload.php' );                 //Media
  //remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'plugins.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'options-general.php' );        //Settings


remove_menu_page( 'edit.php?post_type=featured_on');
  remove_menu_page( 'vc-welcome' );
//remove_submenu_page( 'edit.php?post_type=product','edit.php?post_type=product&amp;page=product_importer' );
//remove_submenu_page( 'edit.php?post_type=product','post-new.php?post_type=product' );
remove_submenu_page( 'edit.php?post_type=product','edit.php?post_type=product&amp;page=product_exporter' );
remove_submenu_page( 'edit.php?post_type=shop_order','admin.php?page=checkout_form_designer' );
remove_submenu_page( 'edit.php?post_type=shop_order','admin.php?page=wc-settings' );
remove_submenu_page( 'edit.php?post_type=shop_order','admin.php?page=wc-status' );
remove_submenu_page( 'edit.php?post_type=shop_order','admin.php?page=wc-addons' );


}
  
}
add_action( 'admin_menu', 'remove_menus' );

