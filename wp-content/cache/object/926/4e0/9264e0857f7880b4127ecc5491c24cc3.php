�B=Z<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:2653;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-12-15 11:46:25";s:13:"post_date_gmt";s:19:"2017-12-15 11:46:25";s:12:"post_content";s:3571:"As a competent strength coach or personal trainer, you already know a wide array of effective training methods and ways to train. As such many of your clients likely had a very high level of success following your programs. But you likely also found out that a program that works great for a client might not work at all for another one; a plan that is highly motivating and fun to do with someone might kill another person’s motivation to train.

Such is the reality of personal training. The fact is that people are different and if you want maximum results (as well as client fidelity and long term commitment) with them you need to build a program or system that fits their own needs. And I’m not just talking about their goals and objectives but rather about how their body and brain will respond to training.

After a long time of working with a client we can come up with many answers regarding what is best for that person. But in this seminar, you will learn how to shoot at the target, or very close to it, right from the start. Saving you and your client a lot of time finding out their optimal way to train and eat.

We will see how to plan a training program and diet based on someone’s neurological profile as well as their physique type. This is the next level of training individualisation and is the future of personal coaching.

<strong>1. INTRODUCTION AND FOUNDING PRINCIPLES</strong>
1.1. Role of dopamine
1.2. Role of serotonine
1.3. Role of noradrenaline
1.4. Role of acetylcholine
1.5. Role of GABA
1.6. Importance of the dopamine/serotonine ratio
1.7. Link between dopamine / adrenaline
1.8. Stress management, anxiety and cortisol

<strong>2. THE FIVE PROFILES</strong>
2.1. Type 1a (Novelty Seeker / Low Acetylcholine)
2.2. Type 1b (Novelty seeker / High Acetylcholine)
2.3. Type 2a (Reward dependant / High GABA)
2.4. Type 2b (reward dependant / Low GABA)
2.5. Type 3 (Harm avoidance)
2.6. Profile continuum
2.7 Correlation with the 5 elements of Chinese medicine

<strong>3. TRAINING PREPARATION</strong>
3.1. What type of preparation should you focus on?
3.2. How to ramp up/warm-up the big strength lifts?
3.3. How to warm-up for isolation work?

<strong>4. THEORETICAL IDEAL TYPE OF TRAINING FOR EACH PROFILE</strong>
4.1. What should be your general training orientation based on your profile?
4.2. What to do when the client’s goal doesn’t seem to fit the optimal training style?
4.3. What should you do for an hybrid profile?

<strong>5. TRAINING VARIABLES AND YOUR PROFILE</strong>
5.1. Frequency
5.2. Intensity
5.3. Volume
5.4. Density
5.5. Exercise selection
5.6. Training methods
5.7. Variation
5.8. Duration of each training phase
5.9. Progression model
5.10. Peaking and Deload

<strong>6. OPTIMAL NUTRITION FOR EACH TYPE</strong>
6.1. Protein intake
6.2. Carbs intake
6.3. Fats intake
6.4. Meal frequency
6.5. Refeed, cheats, how often and which type?

<strong>7. </strong>SUPLEMENTATION
7.1. Best supplements for type 1a
7.2. Best supplements for type 1b
7.3. Best supplements for type 2a
7.4. Best supplements for type 2b
7.5. Best supplements for type 3
7.6. Peri-workout approach

<strong>8. EXERCISE SELECTION BASED ON BIOMECHANICAL AND NEUROLOGICAL TYPE</strong>
8.1. Leg/Torso ratio and exercise selection
8.2. Tibia/Femur ratio and exercise selection
8.3. Arm/Torso ratio and exercise selection
8.4. Forearm/Upper arm ratio and exercise selection
8.5. Influence of neurological profile on exercise selection

The seminar is 100% theoretical";s:10:"post_title";s:20:"Flagboxing Workshops";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"flagboxing-workshops";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-12-15 13:05:35";s:17:"post_modified_gmt";s:19:"2017-12-15 13:05:35";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:53:"https://flagboxing.com/?post_type=product&#038;p=2653";s:10:"menu_order";i:0;s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}