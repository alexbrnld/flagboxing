<?php 
/**
  *
  * @package Wordpress - xela
  * @subpackage xela
  * @since xela 1.0
  *
**/
get_header(); ?>

<!-- Contents here -->

	<?php  

		echo $cat_id = 24;
		$post_id = get_the_id();

		$args = array(
			'p' => $post_id,
			'cat' => $cat_id,
			'post_type' => $post_type
		);

		$query = new WP_Query($args);

	?>

		<?php if ($query->have_posts()): ?>
			<?php while ($query->have_posts()): ?>
				<?php $query->the_post(); ?>

				<?php  
					$post_title = get_the_title();
					$post_content = apply_filters( 'the_content', get_the_content() );
					$post_featureimage = wp_get_attachment_url( get_post_thumbnail_id() );
				?>
			<?php endwhile ?>
		<?php endif ?>

	<?php wp_reset_query() ?>

	<div class="single-mediacoverage">
		<div class="mc-featureimage" style="background-image: url(<?=$post_featureimage?>)">

			<div class="pattern"></div>

		</div>
		<div class="container news-items" >
			<div class="row">
				<div class="col-md-9">
					<div class="mediacoverage-content">
						
						<div class="mctitle-wrapper">
							<div>
								<h1 class="mc-title"><?=$post_title?></h1>
							</div>
						</div>
						<?=$post_content?>

					</div>
				</div>
				<div class="col-md-3"> 
					<div class="sidebar-latestnews">

						<h3 class="media-thead">More News</h3>

						<?php  

							$args = array(
								'cat' => $cat_id,
								'post__not_in' => array($post_id),
								'post_type' => $post_type,
								'posts_per_page' => 5
							);

							$query = new WP_Query($args);

						?>

						<?php if ($query->have_posts()): ?>
							<?php while ($query->have_posts()): ?>
								<?php $query->the_post(); ?>

								<div class="media">
								  <div class="media-left">
								    <a href="<?=get_permalink()?>" title="<?=get_the_title()?>">
								      <img class="media-object" src="<?=wp_get_attachment_url(get_post_thumbnail_id())?>" alt="<?=get_the_title()?>">
								    </a>
								  </div>
								  <div class="media-body">
								  	<a href="<?=get_permalink()?>" title="<?=get_the_title()?>">
								    	<h4 class="media-heading"><?=get_the_title()?></h4>
									</a>
								    <?=get_the_excerpt()?>
								  </div>
								</div>

							<?php endwhile ?>
						<?php endif ?>

						<?php wp_reset_query() ?>

					</div>
				</div>
			</div>
		</div>
	</div>


<!-- Get footer -->
<?=get_footer()?>