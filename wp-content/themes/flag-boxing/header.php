<?php 
/**
  * This file displays the header for the theme 
  * Displays all of the head element and everything
  *
  *
  * @package Wordpress - xela
  * @subpackage xela
  * @since xela 1.0
  *
**/
?>
<!DOCTYPE html>
<html>
<head>

    <?php 
        $title = "FLAGBOXING";  //Category: Cat Title + Default, Home: Default, Single: Title + Default 
        $keywords = "Empower Women. Realize your potential. Endurance, Stamina, Strength,Flexibility, Power, Speed, coordination, agility, balance, and accuracy. It’s never too late to empower yourself!, No risk no reward. Put on your game face! Saudi Arabia Gym, Gym for Women, Saudi Gym" ; //Category: Cat Title + Default, Home: Default, Single: Title + Default + Tags
        $description = "Take charge of your life and implement change. If you’re inspired to eat healthier, lose weight, sleep better, reduce stress, increase energy, or simply build confidence, then take the initiative and exercise your right at FLAGBOXING."; //Category: Default, Home: Default, Single: Title + Default + Tags
        $image_src = get_stylesheet_directory_uri() . '/assets/img/logo.png'; //Category:  Default, Home: Default, Single: Article Image
        $url = get_stylesheet_directory_uri(); //Category:  Default, Home: Default, Single: author 
        $type = "Website";  
        $sitename = "FLAGBOXING"; 
        $author = "FLAGBOXING"; 
    ?>

    <?php 
        if (is_single()) {
            $p_id =  get_the_id();
            $post_type =  get_post_type();
            $cate = get_the_category($p_id);
            $post_tags = get_the_tags();
            $description = get_the_excerpt($p_id);
            $description = str_replace('"', "", $description);
            if (count($cate) > 0) {
                if ($cate) {
                    foreach ($cate as $key => $cat) {
                       $ctname =$cat->name ." > ";
                    }
                
                    if ($ctname == "Uncategorized") {
                        $cate_name = "FLAGBOXING" ;
                        $keywords = "FLAGBOXING";
                    } else {
                        $cate_name = $ctname;
                        $keywords = "FLAGBOXING - " . $ctname;
                    }
                }
            }
            if ($post_tags) {
                $post_tagsname = $post_tags[0]->name;
            }
            $title = $cate_name . get_the_title();
            if (isset($post_tagsname)) {
                $keywords = "FLAGBOXING," ." ". $cate_name .", ". get_the_title() . ", " . $post_tagsname;
            } else {
                $keywords = "FLAGBOXING," ." ". $cate_name .", ". get_the_title();
            }
            if ($post_type == "class") {
                $image_src = get_post_meta( $p_id, '_wcs_image', true );
            } else {
                $image_src =wp_get_attachment_url( get_post_thumbnail_id($p_id) );
            }

            $args = array('p'=> $p_id);
            $query = new WP_Query($args);

            if ($query->have_posts()):
                while($query->have_posts()):
                    $query->the_post();
                    $author =get_the_author_meta('display_name');
                endwhile;
            endif;
            wp_reset_query();
        }

        if (is_page()) {
            $title = get_the_title() . ' | FLAGBOXING';  
            $image_src =wp_get_attachment_url( get_post_thumbnail_id($p_id) );
        }
    ?>
    <title><?=$title?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <meta description="<?=$description?>">
    <meta keywords="<?=$keywords?>">
    <meta author="<?=$author?>">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="" type="image/x-icon" >
    <link rel="image_src" href="<?=$image_src?>"/>
    <link rel="canonical" href="<?=$image_src?>">

    <!-- GOOGLE -->
    <meta name="google-site-verification" content="" />

    <!-- FACEBOOK -->
    <meta property="og:url" content="<?=$url?>" />
    <meta property="og:type" content="<?=$type?>" />
    <meta property="og:title" content="<?=$title?>" />
    <meta property="og:description" content="<?=$description?>" />
    <meta property="og:image" content="<?=$image_src?>" />
    <meta property="og:image:alt" content="<?=$title?>" />
    <meta property="fb:app_id" content="140900356700863" />

    <!-- TWITTER -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@<?=$sitename?>>">
    <meta name="twitter:title" content='<?=$title?>'>
    <meta name="twitter:description" content='<?=$description?>'>
    <meta name="twitter:image:src" content="<?=$image_src?>">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/html5.js"></script>
    <![endif]-->

    <script type="text/javascript">
    	var arrowRight = "<img src='<?=get_stylesheet_directory_uri()?>/assets/img/arrow-right.png'>";
    	var arrowLeft = "<img src='<?=get_stylesheet_directory_uri()?>/assets/img/arrow-left.png'>";
    	var homeID = <?=get_the_id()?>;
    </script>

    <?=wp_head()?>
</head>
<body <?php body_class(); ?> > 

    <?php  

        // Get our app english (Default) language translations
        $download_title = "Sign up for Classes by Downloading our APP Now!";
        $app_desc = "<p>View Class schedules</p><p>Book your classes</p><p>Check out on going promotions</p><p>Get studio locations</p>
        <h4>Payments made through the website ONLY. Please select and buy any of our packages to get access to all our classes. </h4>";

    ?>
    <style type="text/css">
    </style>

    <!-- COMING SOON -->
    <div class="coming-soon-popup same-close">
        <div class="coming-soon-wrapper">
            <div class="coming-soon-content">
                <a href="javascript:void(0)" id="close-app" class="close-mobile">
                   <i class="fa fa-times" aria-hidden="true"></i>
                </a>
                <div class="comingsoontxt">
                    <h1 id="packages-pop-head">COMING SOON</h1>
                    <a href="<?=site_url()?>" class="pckge-cls">Home</a>
                </div>
            </div>
        </div>
    </div>

    <!-- GET THE APP -->
    <div class="get-the-app same-close">
        <div class="app-wrapper">
            <div class="app-inner">
                <div class="app-content">
                    <div class="row">
                        <div class="col-xs-6 app-left-section">
                            <a href="javascript:void(0)" id="close-app" class="close-mobile">
                               <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="col-xs-6 app-right-section">
                            <a href="javascript:void(0)" id="close-app" class="close-desktop">
                               <i class="fa fa-times" aria-hidden="true"></i>
                            </a>

                            <div class="left-inner">
                                <h2 class="download-title"><?=$download_title?></h2>
                                <div class="app-description">
                                  <?=$app_desc?>
                                 </div>
                                 <a target="_blank" href="https://play.google.com/store/apps/details?id=com.fitnessmobileapps.flagboxing" class="dl-appbtn"><img src="https://s3-ap-southeast-1.amazonaws.com/flagboxing/wp-content/uploads/2017/12/17170701/google-play-button-250x84.png"></a>
                                 <a target="_blank" href="https://itunes.apple.com/us/app/flagboxing/id1075266584" class="dl-appbtn"><img src="https://s3-ap-southeast-1.amazonaws.com/flagboxing/wp-content/uploads/2017/12/17170708/1280px-Download_on_the_App_Store_Badge.svg_-250x74.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <header>

    	<!-- DESKTOP -->
        <div class="fl-header-wrapper scrolled">
            <div class="call-inner">
                <ul class="social-inHd">
                    <li>
                        <a class="vc_icon_element-link" href="tel:+966506378539" title="Call us" target=" _blank">
                            <span class="vc_icon_element-icon fa fa-phone"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="fl-header">
                <div class="mobile-bars">
                    <a class="nav-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <div class="fbx-wrappermn">
                    <div class="fbwrapperinner">
                        <div class="wrapperin">                    
                            <?php  
                                wp_nav_menu( array(
                                    'theme_location'  => 'primary-menu',
                                    'container'       => false,
                                    'menu_class'      => 'fbx-menu',
                                ) );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="menu-navigation">
                    <div class="flagbx-menu menu-left-nav">                
                        <?php  
                            wp_nav_menu( array(
                                'theme_location'  => 'left-nav-menu',
                                'container'       => false,
                                'menu_class'      => 'left-menu',
                            ) );
                        ?>    
                    </div>
                    <div class="site-logo">
                        <a href="<?=get_site_url()?>" title="Flag Boxing logo" class="flag-boxing-logo">
                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/flagboxing_logo.png">
                        </a>
                    </div>
                    <div class="flagbx-menu menu-right-nav">               
                        <?php  
                            wp_nav_menu( array(
                                'theme_location'  => 'right-nav-menu',
                                'container'       => false,
                                'menu_class'      => 'right-menu',
                            ) );
                        ?>
                    </div>
                </div>
                <!-- Language Wrap -->
            </div>
            <div class="right-top-wrapper">

            	<ul class="social-inHd">
                    <li>
                        <a class="vc_icon_element-link" href="https://www.facebook.com/flagboxing/" title="" target=" _blank">
                            <span class="vc_icon_element-icon fa fa-facebook"></span>
                        </a>
                    </li>
            		<li>
                        <a class="vc_icon_element-link" href="https://www.instagram.com/flagboxing/" title="" target=" _blank">
                            <span class="vc_icon_element-icon fa fa-instagram"></span>
                        </a>
            		</li>
            		<li>
                        <a class="vc_icon_element-link" href="#" title="" target=" _blank">
                            <span class="vc_icon_element-icon fa fa-snapchat-ghost"></span>
                        </a>
            		</li>
                    <li>
                        <a class="vc_icon_element-link" href="#" title="" target=" _blank">
                            <span class="vc_icon_element-icon fa fa-youtube"></span>
                        </a>
                    </li>
            		<li>
                        <a class="vc_icon_element-link" href="#" title="" target=" _blank">
                            <span class="vc_icon_element-icon fa fa-twitter"></span>
                        </a>
            		</li>
            	</ul>
            	<ul>
            		<li>
		            	<div class="cart-secttion">
		            		<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		 
							    $count = WC()->cart->cart_contents_count;
							    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
							    if ( $count > 0 ) {
							        ?>
							        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
							        <?php
							    }
							        ?></a>
							 
							<?php } ?>
		            	</div>
            		</li>
            	</ul>
            	<ul>
                    <li>
                        <div class="language_menu">            
                            <?php  
                                // echo do_shortcode('[wpml_language_selector_widget]');
                            ?>    
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div class="default_sitecontent">
