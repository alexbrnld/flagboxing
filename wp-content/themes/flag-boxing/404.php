
<?php 
/**
  * This file displays the header for the theme 
  * Displays all of the head element and everything
  *
  *
  * @package Wordpress - xela
  * @subpackage xela
  * @since xela 1.0
  *
**/

// Get header
get_header(); ?>	
	<div class="container err_page">
		<div class="row">
			<div class="col-md-12">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

						<section class="error-404 not-found">
							<div>
								<h1 class="page-title text-center">404 ERROR</h1>
							</div>
							<br>
							<div class="page-content_f  text-center">
								SORRY PAGE NOT FOUND. <br>
								<a class=" text-center" href="<?=get_site_url()?>" title="Home">Home</a>
							</div><!-- .page-content -->
						</section><!-- .error-404 -->
					</main><!-- .site-main -->
				</div><!-- .content-area -->
			</div>
		</div>
	</div>
	
<!-- Get footer -->
<?=get_footer()?>