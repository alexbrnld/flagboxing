<?php 
/**
  *
  * @package Wordpress - xela
  * @subpackage xela
  * @since xela 1.0
  *
**/
get_header(); ?>

<!-- Contents here -->
	
	<?php  



		$post_id = get_the_id();
		$categories = get_the_category();
		$catID = $categories[0]->term_id;
		
		$cat_id = 24;
		$categories = get_categories($post_id);

		if ($catID == 24) { ?>
			<div class="single-mediacoverage">
				<div class="container news-items" >
					<div class="row">
						<div class="col-md-12">

						<?php if (have_posts()): ?>
							<?php while (have_posts()): ?>
								<?php the_post(); ?>
							
									<div class="mediacoverage-content">
										
										<div class="mctitle-wrapper">
											<div>
												<h1 class="mc-title"><?php echo get_the_title(); ?></h1>
											</div>
										</div>
										<?php echo apply_filters( 'the_content', get_the_content() ); ?>

									</div>
								<?php endwhile ?>
							<?php endif ?>

						<?php wp_reset_query() ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							
							<div class="sidebar-latestnews">

			                    <?php if (ICL_LANGUAGE_CODE=="en"): ?>
									<h3 class="media-thead-n">More News</h3>
			                     <?php else: ?>
									<h3 class="media-thead-n">المزيد من الأخبار</h3>
			                    <?php endif ?>

								<?php  

									$args = array(
										'cat' => $cat_id,
										'post__not_in' => array(get_the_id()),
										'post_type' => $post_type,
										'posts_per_page' => 5
									);

									$query = new WP_Query($args);

								?>

								<div class="row">
									

									<?php if ($query->have_posts()): ?>
										<?php while ($query->have_posts()): ?>
											<?php $query->the_post(); ?>

											<div class="col-md-4">
												<div class="more-news-items">
												    <a href="<?=get_permalink()?>" title="<?=get_the_title()?>">
												      <img class="img-responsive" src="<?=wp_get_attachment_url(get_post_thumbnail_id())?>" alt="<?=get_the_title()?>">
												    </a>
												    <br>
												  	<a href="<?=get_permalink()?>" title="<?=get_the_title()?>">
												    	<h4 class="media-heading"><?=get_the_title()?></h4>
													</a>
												    <?=get_the_excerpt()?>
												</div>
											</div>
											<!-- <div class="media">
											  <div class="media-left">
											  </div>
											  <div class="media-body">
											  </div>
											</div> -->

										<?php endwhile ?>
									<?php endif ?>

									<?php wp_reset_query() ?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<?php } else {
				if (have_posts()) {
					while (have_posts()) {
						the_post();

						the_content();

					}
				}
			}



		
	?>


<!-- Get footer -->
<?=get_footer()?>