<!-- OVERRIDE SINGLE - PRODUCT -->

<?=get_header()?>

	<?php  

		// PRODUCT DATA
		global $product;
		$product_name = get_the_title();
		$product_id = get_the_id();
		$instructor = get_field('instructor');
		$date_and_time = get_field('date_and_time');
		$location_name = get_field('location_name');
		$location_map = get_field('location_map');
		$short_description = get_field('short_description');
		$content = get_the_content();
		$long_description = apply_filters( 'the_content', $content );
		$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
		$price = $product->get_price_html();
	?>

	<div class="pg-product-single">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Top product section -->
					<div class="row">
						<div class="col-md-6">
							<div class="pr-img">
								<img src="<?=$feat_image_url?>" class="img-responsive" alt="<?=$product_name?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="more-info">
								<div class="pr--title-wrapper">
									<h1 class="pr--title"><?=$product_name?></h1>
								</div>
								<div class="pr--price-wrapper">
									<h3 class="pr--price"><?=$price?></h3>
								</div>
								<div class="pr--instructor-wrapper">
									<h3 class="pr--instructor">Instructor:</h3><h3> <?=$prinstructorice?> </h3>
								</div>
								<div class="pr--short_description-wrapper">
									<p class="pr--short_description"> 
										<?=$short_description?>
									</p>
								</div>
								<div class="pr--date_and_time-wrapper">
									<h3 class="pr--label">Date &amp; Time</h3>
									<p class="pr--date_and_time"> 
										<?=$date_and_time?>
									</p>
								</div>
								<div class="pr--location-wrapper">
									<h3 class="pr--label">Location</h3>
									<p class="pr--location"> 
										<?=$location_name?>
									</p>
								</div>
								<div class="pr--location-wrapper">
									<h3 class="pr--label">Price</h3>
									<p class="pr--pricec"> 
										<?=$date_and_time?>
									</p>
								</div>
								<div class="pr--addtocart-wrapper">
									<h3 class="pr--label">Price</h3>
									<div class="pr--addtocart"> 
										<?=do_shortcode('[add_to_cart id="'.$product_id.'"]');?>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


<?=get_footer()?>

